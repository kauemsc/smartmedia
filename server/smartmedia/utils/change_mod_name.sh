#!/bin/bash

# Este script modifica o nome do modulo de $name para $newname

name=Softphone
newname=Portal
name2=softphone
newname2=portal

find -type f ../ | while read file
	do 
		echo -en "Procurando e alterando em $file ..." 
		cat $file|sed s/$name/$newname/g |sed s/$name2/$newname2/g > ${file}.new 
		mv ${file}.new $file 
		echo done 
	done

