<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contacts
 *
 * @author elton
 */
class SmartMedia_Upload_Manager {

    /*
     * Manutenção registro referente group
     */

    // retorna todos group
    public static function getAll() {

        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from ('smartmedia_files');

        $stmt = $db->query ($select);
        $row = $stmt->fetchAll();
        
        $groups = array();
        foreach ($row as $group) {
            $groups[$group['id']] = $group['name'];
        }
        return $groups;
    }
 	public static function getAllFilter($query, $field, $id) {

                $db = Zend_registry::get('db');


                if ($query != null) {
                        if ($field == "name") {
                                $clientsData = $db->query("select * from smartmedia_files where groupId = '$id' and name like '%$query%'")->fetchAll();
                        }
                }else{
                        $clientsData = $db->query("select * from smartmedia_files where groupId = '$id' ")->fetchAll();
                }
                return $clientsData;
        }

    // retorna campanha
    public static function get ($id) {

        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from ('smartmedia_files')
                ->where ("id = '$id'");

        $stmt = $db->query($select);
        $registros = $stmt->fetch();

        return $registros;
    }

    public static function getIdByName($name) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from ('sms_group')
                ->where ("name = '$name'");

        $stmt = $db->query($select);
        $registros = $stmt->fetch();

        return $registros['id'];
    }

    // retorna group - filtro
    public static function getFilter ($field, $query) {

        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from ('sms_group', array('id','name'));

        if (!is_null($query)) {
                $select->where ("$field like '%$query%'");
        }

        $stmt = $db->query($select);
        $groups = $stmt->fetchAll();

        $sms_groups = array();
        foreach($groups as $id => $group) {
            $sms_groups[$id] = $group;
            $sms_groups[$id]['contacts'] = self::getContacts($group['id']);
        }
        
        return $sms_groups;
    }

    public static function getContacts($group_id) {
        
        $db = Zend_Registry::get('db');
        
        $select = $db->select()
                     ->from('sms_group_contact', array('count(contact) as contacts'))
                     ->where('sms_group_contact.group = ?', $group_id);
        $stmt = $db->query($select);
        $result = $stmt->fetch();

        return $result['contacts'];
    }

    // insere group
    public static function add($file) {
        $db = Zend_Registry::get('db');
        $db->insert('smartmedia_files', $file);
        return $db->lastInsertId();
    }

    // edita group
    public static function  edit($file,$id) {
        
	$db = Zend_Registry::get('db');
        $db->beginTransaction();
        
        try {

            $db->update ('smartmedia_files', $file, 'id ='.$id);
            $db->commit();
            return true;
        }
        catch(Exception $e) {

            $db->rollBack();
            return $e;
        }
    }

    // exclui um group
    public static function remove($id) {
        $db = Zend_Registry::get('db');
        $db->delete('smartmedia_files', "id = $id");
    }
}
