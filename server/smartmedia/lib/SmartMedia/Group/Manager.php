<?php

/**
 * Client Manager for Clients
 * @author Kaue Santos
 */
class SmartMedia_Group_Manager {
	
	/*
	 * Manutenção registro referente group
	 */
	
	// retorna todos group
	public static function getAll() {
		$db = Zend_Registry::get ( 'db' );
		$select = $db->select ()->from ( 'smartmedia_groups' );
		
		$stmt = $db->query ( $select );
		$row = $stmt->fetchAll ();
		
		$groups = array ();
		foreach ( $row as $group ) {
			$groups [$group ['id']] = $group ['name'];
		}
		return $groups;
	}
	
	/**
	 * Adiciona um grupo
	 * @param array $data
	 */
	public static function add($data) {
		$db = Zend_Registry::get('db');
		$insert_data = array(
				'name'    	=> $data['name']
		);
	
	
		$db->insert('smartmedia_groups', $insert_data);
		return $db->lastInsertId();
	}
	/**
	 * retorna um grupo
	 * @param int $id
	 * @return array da tupla do evento
	 */
	public static function get($id) {
		$db = Zend_Registry::get('db');
	
		$select = $db->select()->from("smartmedia_groups")->where('id = ?', $id);
	
		return $db->query($select)->fetch();
	}
	
	/**
	 * getgroups - Rotina de filtro de busca de grupos
	 *
	 * @return <Array> $groups_list - Array de grupos existentes
	 */
	public static function getAllFilter($query, $field) {
		$db = Zend_registry::get ( 'db' );
	
		if (isset ( $query )) {
			$groups_list = $db->query ( "select id,name from smartmedia_groups where name like '%$query%'" )->fetchAll ();
		} else {
			$groups_list = $db->query ( "select id,name from smartmedia_groups" )->fetchAll ();
		}
		return $groups_list;
	}
	/**
	 * edita um grupo
	 *
	 * @param array $data
	 * @param int $id
	 */
	public static function edit($data, $id) {
		$db = Zend_Registry::get ( 'db' );
		$update_data = array (
				'id' => $id,
				'name' => $data ['name']
		);
	
		$db->update ( "smartmedia_groups", $update_data, array (
				'id = ?' => $id
		) );
	}
	
	/**
	 * Remove grupo pelo id
	 * @param unknown $id
	 */
	public static function remove($id) {
		$db = Zend_Registry::get ( 'db' );
		return $db->delete ( "smartmedia_groups", array (
				"id = ?" => $id
		) );
	}
}
