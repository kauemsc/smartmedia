<?php

/**
 *  This file is part of SNEP.
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SmartMedia Upload Controller
 *
 * @category Snep
 * @package Snep
 * @copyright Copyright (c) 2014 OpenS Tecnologia
 * @author Kaue Santos
 */
class SmartMedia_UploadController extends Zend_Controller_Action {
	
	/**
	 * Lista arquivos de som
	 */
	public function indexAction() {
		
		$this->view->breadcrumb = $this->view->translate ( "SmartMedia » Grupos » Upload Media" );
		
		$id = $this->_request->getParam ( 'id' );
		
		$this->view->group = $id;
		
		if ($this->_request->getPost ( 'filtro' )) {
			$field = $this->_request->getPost ( 'campo' );
			$query = $this->_request->getPost ( 'filtro' );
		}
		if (! isset ( $query )) {
			$query = "";
			$field = "";
		}
		
		$files = SmartMedia_Upload_Manager::getAllFilter ( $query, $field, $id );
		
		$page = $this->_request->getParam ( 'page' );
		$this->view->page = (isset ( $page ) && is_numeric ( $page ) ? $page : 1);
		$this->view->filtro = $this->_request->getParam ( 'filtro' );
		
		$paginatorAdapter = new Zend_Paginator_Adapter_Array ( $files );
		$paginator = new Zend_Paginator ( $paginatorAdapter );
		$paginator->setCurrentPageNumber ( $this->view->page );
		$paginator->setItemCountPerPage ( Zend_Registry::get ( 'config' )->ambiente->linelimit );
		
		$this->view->pathweb = Zend_Registry::get ( 'config' )->system->path->web;
		$this->view->files = $paginator;
		$this->view->pages = $paginator->getPages ();
		$this->view->url = $this->getFrontController ()->getBaseUrl () . "/" . $this->getRequest ()->getModuleName () . "/upload/";
		$this->view->PAGE_URL = $this->getFrontController ()->getBaseUrl () . "/" . $this->getRequest ()->getModuleName () . "/" . $this->getRequest ()->getControllerName () . "/index/id/{$id}/";
		
		$options = array (
				"description" => $this->view->translate ( "Descricao" ),
				"id" => $this->view->translate ( "id" ) 
		);
		
		// Formuláo de filtro.
		$filter = new Snep_Form_Filter ();
		$filter->setAction ( $this->getFrontController ()->getBaseUrl () . "/smartmedia/upload/index/id/{$id}" );
		$filter->setValue ( $this->_request->getPost ( 'campo' ) );
		$filter->setFieldOptions ( $options );
		$filter->setFieldValue ( $this->_request->getParam ( 'filtro' ) );
		$filter->setResetUrl ( "{$this->getFrontController()->getBaseUrl()}/smartmedia/upload/index/id/{$id}" );
		
		$this->view->form_filter = $filter;
		
		$this->view->filter = array (
				array (
						"url" => $this->view->url . "add/id/{$id}",
						"display" => $this->view->translate ( "Incluir Arquivo" ),
						"css" => "include" 
				) 
		);
	}
	
	/**
	 * adiciona arquivo
	 */
	public function addAction() {
		
		$id = $this->_request->getParam ( 'id' );
		
		$this->view->id = $id;
		
		$path = APPLICATION_PATH . '/modules/' . $this->getRequest ()->getModuleName () . "/media/" . $id;
		$path_web = Zend_Registry::get ( 'config' )->system->path->web . '/modules/' . $this->getRequest ()->getModuleName () . "/media/" . $id . "/";
		
		$this->view->breadcrumb = $this->view->translate ( "Smart Media » Grupos  » Uplaod Media » Incluir" );
		
		if ($this->_request->getPost ()) {
			$formData = $this->_request->getPost ();
			exec ( "cp {$_FILES['file']['tmp_name']} $path/{$_FILES['file']['name']}" );
			$upload ['name'] = $_FILES ["file"] ["name"];
			$upload ['description'] = $formData ['description'];
			$upload ['fullPath'] = $path_web . $_FILES ['file'] ['name'];
			$upload ['groupId'] = $id;
			SmartMedia_Upload_Manager::add ( $upload );
			$this->_redirect ( $this->getRequest ()->getModuleName () . '/' . $this->getRequest ()->getControllerName () . "/index/id/{$id}" );
		}
	}
	/**
	 * Edita arquivos
	 */
	public function editAction() {
		
		$id = $this->_request->getParam ( 'id' );
		$id_group = $this->_request->getParam ( 'group' );
		
		$this->view->group = $id_group;
		$this->view->id = $id;
		$this->view->breadcrumb = $this->view->translate ( "Smart Media » Grupos  » Uplaod Media » Editar" );
		
		$dados = SmartMedia_Upload_Manager::get ( $id );
		if ($dados) {
			$this->view->form = $dados;
		}
		if ($this->_request->getPost ()) {
			
			$formData = $this->_request->getPost ();
			if ($_FILES ['file'] ['tmp_name'] != "") {
				
				$path = APPLICATION_PATH . '/modules/' . $this->getRequest ()->getModuleName () . "/media/" . $id_group;
				$path_web = Zend_Registry::get ( 'config' )->system->path->web . '/modules/' . $this->getRequest ()->getModuleName () . "/media/" . $id_group . "/";
				exec ( "rm $path/{$dados['name']}" );
				exec ( "cp {$_FILES['file']['tmp_name']} $path/{$_FILES['file']['name']}" );
				$upload ['name'] = $_FILES ["file"] ["name"];
				$upload ['fullPath'] = $path_web . $_FILES ['file'] ['name'];
			}
			$upload ['description'] = $formData ['description'];
			SmartMedia_Upload_Manager::edit ( $upload, $id );
			$this->_redirect ( $this->getRequest ()->getModuleName () . '/' . $this->getRequest ()->getControllerName () . '/index/id/' . $id_group );
		}
	}
	
	/**
	 * Remove arquivo
	 */
	public function removeAction() {
		
		$id = $this->_request->getParam ( 'id' );
		$id_group = $this->_request->getParam ( 'group' );
		
		SmartMedia_Upload_Manager::remove ( $id );
		$this->_redirect ( $this->getRequest ()->getModuleName () . '/' . $this->getRequest ()->getControllerName () . '/index/id/' . $id_group );
	}
}
