<?php

/**
 * Classe DialService - Efetua uma ligação entre um ramal ou agente e uma extensão, faz com 
 * que o telefone do ramal/agente toque e após o atendimento ele é direcionado para a extensão
 * desejada, é como se o ramal estivesse discando aquele numero.
 * 
 * @category  Services
 * @package   services_DiaService
 * @author Opens Tecnologia
 */
require_once("../lib/Kiper/Gate/Manager.php");
class GateService implements SnepService {

    /**
     * execute - Executa as ações do serviço
     * @return <array>
     */
    public function execute() {
	global $param; 
	$db = Zend_Registry::get('db');
        // buscando as configurações do snep
        $config = Zend_Registry::get('config');

        // Verificando parametros
        if (!isset($_GET['gate'])) {
            error("Falta parametro: gate");
        }
        if (!isset($_GET['ramal'])) {
            error("Falta parametro: ramal");
        }


        if (isset($_GET['gate'])) {
            // resolvendo portao
            $gate = Kiper_Gate_Manager::getAllFilter($_GET['gate'],"extension");
        }
        if (isset($_GET['ramal'])) {
            // resolvendo ramal
	    try {
            	$ramal = PBX_Usuarios::get($_GET['ramal']);
	    }catch(Exception $ex){
		    error($ex->getMessage());
	    }

        }

        if (isset($_GET['userfield'])) {
            $userfield = $_GET['userfield'];
        } else {
            $userfield = implode("_", array(
                time(),
                date("Ymd"),
                date("Hi"),
                $gate[0]['extension']
                    ));
        }
	$this->gatechannel = $gate[0]['channel'];



        // criando conexão com o asterisk
        $asterisk = new Asterisk_AMI();
        $asterisk->connect($config->ambiente->ip_sock, $config->ambiente->user_sock, $config->ambiente->pass_sock);


        // Enviando requisição de status
        $asterisk->Status('');
        // Enviando esse objeto para cuidar dos responses
        $asterisk->wait_event($this);

        // se foi encontrado o canal para o hangup
/*
        if (isset($this->channelopened)) {
		$parameters = array(
			"Channel" 	=> "{$this->channelopened}",
			"Digit"		=> "*"
		);
		$asterisk->send_request("PlayDTMF",$parameters);
		sleep(1);
		$parameters = array(
			"Channel" 	=> "{$this->channelopened}",
			"Digit"		=> "5"
		);
		$asterisk->send_request("PlayDTMF",$parameters);
		sleep(1);
		$parameters = array(
			"Channel" 	=> "{$this->channelopened}",
			"Digit"		=> "2"
		);
		$asterisk->send_request("PlayDTMF",$parameters);

            	return array("status" => "success", "message" => "Canal ja em uso: {$this->channelopened} com {$this->link}", "userfield" => $userfield);
	} else {
*/
            // enviando requisição de originate
            $answer = $asterisk->Originate(
                    'Local/'.$gate[0]['extension'].'@default', // Channel
                    //$this->canal, // Channel
                    null, // Exten
                    null, // Contexto
                    null, // Prioridade
                    'AGI', // Application
                    'snep/opengate.php', // data
                    50000, // Timeout
                    "opengate <opengate>", // Callerid(name)?
                    null, // Variaveis
                    null, // Accountcode
                    true                             // Async
            );

	    $sql = "INSERT INTO `services_log` VALUES(NOW(), '$ramal', '{$gate[0]['namekeeper']}', True, '{$gate[0]['namekeeper']}/{$gate[0]['name']} foi aberto')";
	    $db->query($sql);
            // Se nenhum erro ocorreu até aqui podemos dizer que foi tudo ok
            return array("status" => "ok", "message" => "requisicao enviada", "userfield" => $userfield, "gate" => $gate[0]['extension'], "channel" => $param['Channel']);
//        }
    }

    /**
     * processEvent - Metodo que faz o tratamento dos eventos de status.
     * @param <String> $event - Evento que esta sendo processado
     * @param <Array> $param - parametros que vieram com o evento
     * @return <boolean> - true se o processamento de eventos deve parar.
     */
    function processEvent($event, $param) {
        if ($event == "status") {
            if (eregi($this->gatechannel, $param['Channel'])) {
                $this->channelopened = $param['Channel'];
		$this->link = $param['Link'];
            }
        } else if ($event == "statuscomplete")
            return true;
    }

}
