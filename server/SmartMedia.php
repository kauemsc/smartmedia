<?php

/**
 * Descritor do módulo SmartMedia.
 *
 * @category  Snep
 * @package   Snep_Bootstrap
 * @copyright Copyright (c) 2012 OpenS Tecnologia
 * @author    Douglas Conrad
 */
class SmartMedia extends Snep_Module_Descriptor {

    public function __construct() {
        $this->setName("SmartMedia");
        $this->setVersion("0.1");
        $this->setDescription("Módulo SmartMedia");
        $this->setModuleId('smartmedia');
        
        $smartmedia = new Snep_Menu_Item('Snep_SmartMedia', "Sistema de Midia", "smartmedia/clients/");
        
        
        $menu = new Snep_Menu_Item("15","SmartMedia", null,array(
                    "smartmedia" => $smartmedia));   
	
        $menu->setResourceId(14);
        $this->setMenuTree( array("smartmedia" => $menu) );

        // Module include_path
        set_include_path(get_include_path() . PATH_SEPARATOR . Zend_Registry::get("config")->system->path->base . "/modules/smartmedia/lib");
        Zend_Loader_Autoloader::getInstance()->registerNamespace("SmartMedia_");

    }

}

